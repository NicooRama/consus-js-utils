import {objectToQueryParam, ObjectUtils} from "../ObjectUtils";

export const normalizeFilters = (filters) => {
    return !ObjectUtils.isEmpty(filters) ? objectToQueryParam(filters) : undefined;
}
