const numbers=["0","1","2","3","4","5","6","7","8","9"];

const removeAccents = (str) => str.normalize('NFD').replace(/[\u0300-\u036f]/g, "");

const normalize = (str) => removeAccents(str.toLowerCase());

const normalizedCompare = (str1, str2) => {
    if(!str1 && !str2) return true;
    if((str1 && !str2) || (!str1 && str2)) return false;
    return removeAccents(str1.toLowerCase()) === removeAccents(str2.toLowerCase());
};

const firstCapitalize = (str) => {
    if(!str) return "";
    return  str[0].toUpperCase() + str.substring(1)
};

const lastCaracter = (str) => str[str.length-1];

const removeLastChar = (str) => str.substring(0,str.length-1);

const isEmpty = (str) => !str || str==='';

const count = (str,caracter) => {
    let result = 0, i = 0;
    for(i;i<str.length;i++)if(str[i]===caracter)result++;
    return result;
};

const isCharNumber = (caracter)=> numbers.indexOf(caracter)!==-1;

const toNumber = (str) => Number(str.replace(",","."));

const isString = (str) => typeof str === 'string';

const stringArrayToText = (phrases: string[], connector = 'y') => {
    phrases = [...phrases];
    let lastPhrase;
    if(phrases.length > 1) {
        lastPhrase = phrases.pop()
    }
    let text = phrases.join(', ')
    if(lastPhrase) {
        text += ` ${connector} ${lastPhrase}`
    }
    return text;
}

const emojisRegex = /[\uD800-\uDBFF][\uDC00-\uDFFF]|\u200D|\uFE0F|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|\uD83E[\uDC00-\uDFFF]|[\u2600-\u27FF]/g;
const removeEmojis = (text: string) => {
    // Remove emojis from the text using the regular expression
    return text.replace(emojisRegex, '');
}

const depluralize = (plural: string): string => {
    const rules: Array<[RegExp, string]> = [
        [/es$/, ''], // Plural masculino/femenino regular
        [/ces$/, 'z'], // Plural masculino/femenino terminado en -z
        [/ces$/, 'z'], // Plural masculino/femenino terminado en -z
        [/s$/, ''], // Plural neutro regular
        [/res$/, 'r'], // Plural neutro terminado en -r
        [/es$/, 'e'], // Plural neutro terminado en -e
    ];

    let result = plural;
    rules.forEach(([pattern, replacement]) => {
        if (pattern.test(result)) {
            result = result.replace(pattern, replacement);
        }
    });

    return result;
};

const normalizePluralization = (word: string): string => {
    if (word.startsWith("1 ")) {
        const str = word.substring(2);
        return `1 ${depluralize(str)}`;
    }
    return word;
}

export function stringToBoolean(str: string) {
    if(!str) return false;
    return str.toLowerCase() === 'true';
}

export function extractBetween(text: string, word1: string, word2: string) {
    const firstIndex = text.indexOf(word1);
    if(firstIndex === -1) return '';
    const start = firstIndex + word1.length;
    if(!word2) {
        return text.substring(start);
    }
    const end = text.indexOf(word2, start);
    if(end === -1) return '';
    return text.substring(start, end);
}

export const numberToRoman = (num: string | number): string => {
    if (typeof num === 'string') {
        num = parseInt(num);
    }
    if (num < 1) {
        throw new Error('numberToRoman only accepts numbers greater than 0');
    }
    if (num > 3999) {
        throw new Error('numberToRoman only accepts numbers less than 4000');
    }
    const romanNumerals: { [key: number]: string } = {
        1: 'I',
        4: 'IV',
        5: 'V',
        9: 'IX',
        10: 'X',
        40: 'XL',
        50: 'L',
        90: 'XC',
        100: 'C',
        400: 'CD',
        500: 'D',
        900: 'CM',
        1000: 'M'
    };
    const romanNumeralKeys = Object.keys(romanNumerals).map(Number).reverse();
    let romanNumeral = '';
    romanNumeralKeys.forEach(key => {
        while ((num as number) >= key) {
            romanNumeral += romanNumerals[key];
            (num as number) -= key;
        }
    });
    return romanNumeral;
}


export const StrUtils = {
    stringToBoolean,
    extractBetween,
    numberToRoman,
    depluralize,
    normalizePluralization,
    removeAccents,
    normalize,
    normalizedCompare,
    firstCapitalize,
    lastCaracter,
    removeLastChar,
    isEmpty,
    count,
    isCharNumber,
    toNumber,
    isString,
    stringArrayToText,
    removeEmojis
};
