import {StrUtils} from "./StrUtils";

describe('StrUtils', () => {
    describe('stringArrayToText', () => {
        it('converts ["foo"] to "foo" successfully', () => {
            const phrases = ['foo']
            expect(StrUtils.stringArrayToText(phrases)).toEqual('foo')
        });
        it('converts ["foo","bar"] to "foo y bar" successfully', () => {
            const phrases = ['foo', 'bar'];
            expect(StrUtils.stringArrayToText(phrases)).toEqual('foo y bar')
        });
        it('converts ["foo","bar","zaz"] to "foo, bar y zaz" successfully', () => {
            const phrases = ['foo', 'bar', 'zaz']
            expect(StrUtils.stringArrayToText(phrases)).toEqual('foo, bar y zaz')
        });
    });

    describe('removeEmojis', () => {
        it('should remove emojis successfully', () => {
            expect(StrUtils.removeEmojis('🦈Frescos')).toEqual('Frescos');
        });
        it('should remove other emojis successfully', () => {
            expect(StrUtils.removeEmojis('🦐REBOZADOS CRUDOS')).toEqual('REBOZADOS CRUDOS');
        });
    });

    describe('stringToBoolean', () => {
        it('returns false when input is undefined', () => {
            expect(StrUtils.stringToBoolean(undefined)).toEqual(false);
        });

        it('returns false when input is null', () => {
            expect(StrUtils.stringToBoolean(null)).toEqual(false);
        });

        it('returns false when input is an empty string', () => {
            expect(StrUtils.stringToBoolean('')).toEqual(false);
        });

        it('returns false when input is a string "false"', () => {
            expect(StrUtils.stringToBoolean('false')).toEqual(false);
        });

        it('returns true when input is a string "true"', () => {
            expect(StrUtils.stringToBoolean('true')).toEqual(true);
        });

        it('returns false when input is a string "False"', () => {
            expect(StrUtils.stringToBoolean('False')).toEqual(false);
        });

        it('returns true when input is a string "True"', () => {
            expect(StrUtils.stringToBoolean('True')).toEqual(true);
        });
    });

    describe('extractBetween', () => {
        it('extracts the text between two words correctly', () => {
            const text = 'Hello, my name is John Doe';
            const word1 = 'my name is ';
            const word2 = ' Doe';
            expect(StrUtils.extractBetween(text, word1, word2)).toEqual('John');
        });

        it('returns an empty string when the first word is not found', () => {
            const text = 'Hello, my name is John Doe';
            const word1 = 'my surname is ';
            const word2 = ' Doe';
            expect(StrUtils.extractBetween(text, word1, word2)).toEqual('');
        });

        it('returns an empty string when the second word is not found', () => {
            const text = 'Hello, my name is John Doe';
            const word1 = 'my name is ';
            const word2 = ' Smith';
            expect(StrUtils.extractBetween(text, word1, word2)).toEqual('');
        });

        it('returns the remaining text when the second word is not provided', () => {
            const text = 'Hello, my name is John Doe';
            const word1 = 'my name is ';
            const word2 = '';
            expect(StrUtils.extractBetween(text, word1, word2)).toEqual('John Doe');
        });

        it('returns an empty string when the text is empty', () => {
            const text = '';
            const word1 = 'my name is ';
            const word2 = ' Doe';
            expect(StrUtils.extractBetween(text, word1, word2)).toEqual('');
        });
    });
})
