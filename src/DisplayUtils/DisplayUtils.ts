import {normalizeAccessor} from "../ObjectUtils/ObjectUtils";
import {StrUtils} from "../StrUtils/index";

export const displayString = (item: string): string => item;
export const displayObject = (item: object, property: string): string => normalizeAccessor(item,property);

/**
 * * Genera el display apropiado segun el tipo de elemento y que display recibio
 * @param displayForm: puede ser una funcion, una property o nada.
 * @returns {Function|(function(*): *)}
 */
export const normalizeDisplay = (displayForm?): (any)=> string => {
    if(!displayForm) return displayString;
    /**Si es una string, es el string de la property del objeto**/
    if(StrUtils.isString(displayForm)) return (item) => displayObject(item, displayForm);
    if(typeof displayForm === "function") return displayForm;
};