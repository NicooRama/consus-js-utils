
const execSum = (data) => data.reduce((accumulator, currentValue) => {
    return accumulator + currentValue
}, 0);

export const sum = (data: (number | any)[], property?: string) => {
    if(!property) {
        return execSum(data);
    }
    return execSum(data.map((d) => d[property]));
}

export const roundToTwoDecimals = (num: number): number => Math.round(num * 100) / 100;

export const roundToNext5 = (x: number) => Math.ceil(x/5)*5
export const roundToPrev5 = (x: number) => Math.floor(x/5)*5

export const multiplesOf5 = (min: number, max: number) => {
    const multiples = [];
    for(let i = min; i <= max; i = i+5){
        multiples.push(i);
    };
    return multiples;
}

export const renderAmount = (amount: number) => {
    let stringAmount = amount.toLocaleString('es-AR', { maximumFractionDigits: 2, minimumFractionDigits: 2 });
    stringAmount = stringAmount.endsWith(",00") ? stringAmount.slice(0, -3) : stringAmount;
    return stringAmount;
}
