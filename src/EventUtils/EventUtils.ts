export const isMiddleClick = (e: any) => {
    return e && (e.which === 2 || e.button === 1 )
}
