/**TODO: cambiar para pasar todo a moment (agregar locale)**/

import moment from "moment";
let dateFormat = require("dateformat");

dateFormat.i18n = {
    dayNames: [
        'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab',
        'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'
    ],
    monthNames: [
        'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic',
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ],
    timeNames: [
        'a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'
    ]
};

const startOfDayMoment = date => moment(date).startOf('day');

const padStart = (number, cantPad = 2) => number.toString().padStart(cantPad, "0");

const toISOFormat = (year,month,day,hour = "00",minutes = "00",seconds = "00",milliseconds = "000") => `${year}-${month}-${day}T${hour}:${minutes}:${seconds}.${milliseconds}Z`;

const toSend = (date) => {
    const year = date.getFullYear();
    const month = padStart(date.getMonth() + 1);
    const day = padStart(date.getDate());
    const hour = padStart(date.getHours());
    const minutes = padStart(date.getMinutes());
    const seconds = padStart(date.getSeconds());
    const milliseconds = padStart(date.getMilliseconds(),3);
    const stringDate = toISOFormat(year,month,day,hour,minutes,seconds,milliseconds);
    return stringDate;
};

const onlyDateToSend = (date) => {
    const year = date.getFullYear();
    const month = padStart(date.getMonth() + 1);
    const day = padStart(date.getDate());
    const stringDate = toISOFormat(year,month,day);
    return stringDate;
};


const toDate = (isoString) => {
  const partes = isoString.split("T");
  const dateParts = partes[0].split("-");
  const hourParts = partes[1].split(":");
  const date = new Date(Number(dateParts[0]),Number(dateParts[1]-1),Number(dateParts[2]),Number(hourParts[0]),Number(hourParts[1]),Number(hourParts[2].substring(0,2)));
  return date;
};

//normaliza el campo "fecha" o el especificado, de un objeto
const normalizeDate = (entity, property = "fecha") => entity[property] = toDate(entity[property]);

const startOfDay = date => startOfDayMoment(date).toDate();
export const MyDateUtils = {
    normalizeDate,
    toDate,
    toSend,
    onlyDateToSend,
    toArgFormat: date => dateFormat(moment(date).toDate(),"dd/mm/yyyy HH:MM"),
    toReadableFormat: date => dateFormat(date,"dddd, dd 'de' mmmm 'de' yyyy"),
    toHourFormat: date => dateFormat(date,"HH:MM"),
    toOnlyDatePart: date => new Date(date.getFullYear(),date.getMonth(),date.getDate()),
    substractDays: (date,days) => moment(date).subtract(days, 'days').toDate(),
    addDays: (date,days) => moment(date).add(days, 'days').toDate(),
    startOfDay,
    endOfDay: date => moment(date).endOf('day').toDate(),
    isSameDate: (date1, date2) => moment(startOfDayMoment(date1)).isSame(startOfDayMoment(date2)),
    isSameHour: (date1, date2) => moment(date1).isSame(date2,'hour') && moment(date1).isSame(date2,'minute'),
};
