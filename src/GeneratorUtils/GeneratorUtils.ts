let lastIdGenerated: number = -1;

export const generateDomId = (): string => {
    lastIdGenerated++;
    return `id${lastIdGenerated}`;
};