import {
    objectPropertyToArray,
    objectToPropertiesArray,
    propertyArrayToQueryParam
} from "./ObjectUtils";

describe('ObjectUtils', () => {
    describe('objectPropertyToArray', () => {
        it('parse {foo: name: "bar"}  to foo.name=bar', () => {
            expect(objectPropertyToArray({
                foo: {
                    name: 'bar'
                }
            }, [])).toEqual(['foo', 'name', 'bar'])
        })
    });

    describe('objectToObjectPropertyArrays', () => {
        it('parse {foo: name: "bar", foo2: name: "bar2"}  to [[foo, name, bar][foo2, name, bar2]]', () => {
            expect(objectToPropertiesArray({
                foo: {
                    name: 'bar'
                },
                foo2: {
                    name: 'bar2'
                }
            })).toEqual([['foo', 'name', 'bar'], ['foo2', 'name', 'bar2']])
        })
    })

    describe('propertyArrayToQueryParam', () => {
        it('parses [\'foo\', \'name\', \'bar\'] to "foo.name=value"', () => {
            expect(propertyArrayToQueryParam(['foo', 'name', 'bar'])).toEqual('foo.name=bar')
        })
    })
})
