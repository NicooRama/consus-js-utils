const isObjectEmpty = (obj: object): boolean => Object.entries(obj).length === 0 && obj.constructor === Object;

/** A traves de un string que contiene la accessor (Ej: foo.sarasa.ne) obtiene el valor(foo: {sarasa:{nee}})
 */

export const normalizeAccessor = (baseObject, accessor, limit = 0): any => {
    const parts = accessor.split(".");
    let finalValue = baseObject[parts[0]];
    for(let i=1; i<parts.length - limit; i++){
        if(finalValue){
            finalValue = finalValue[parts[i]];
        }else{
            break;
        }
    }
    return finalValue;
};

export const normalizeContainerAccessor = (baseObject, accessor): any => normalizeAccessor(baseObject, accessor, 1);

export const lastAccessor = (accessor: string): string => accessor.split(".").pop();

export const isObject = (value: any) => typeof value === 'object' &&
    !Array.isArray(value) &&
    value !== null

export const objectPropertyToArray = (objectOrValue: any, keysAcummulator: string[]): any[] => {
    if(isObject(objectOrValue)){
        const keys = Object.keys(objectOrValue);
        const key = keys[0]
        keysAcummulator.push(key)
        return objectPropertyToArray(objectOrValue[key], keysAcummulator);
    } else {
        keysAcummulator.push(objectOrValue)
        return keysAcummulator;
    }
}

export const objectToPropertiesArray = (object: any) => {
    if(!isObject(object)) {
        return []
    };
    return Object.keys(object).map((key) => objectPropertyToArray(object[key], [key]));
}

export const propertyArrayToQueryParam = (array: any[]) => {
    const parts = [...array];
    const value = parts.pop();
    return parts.join('.') + '=' + value;
}

export const objectToQueryParam = (object: any) => {
    const propertiesArray = objectToPropertiesArray(object);
    return propertiesArray.map((properties) => propertyArrayToQueryParam(properties)).join(',')
}

export const deleteUndefinedProps = (object: any) => {
    const normalizedObject = {...object};
    Object.keys(normalizedObject).forEach((key) => {
        if(normalizedObject[key] === undefined) {
            delete normalizedObject[key]
        }
    });
    return normalizedObject;
}

export const clone = (object: any) => JSON.parse(JSON.stringify(object));

export function getIn(object: any, keys: string | string[], defaultVal?: any) {
    keys = Array.isArray(keys) ? keys : keys.split('.');
    object = object[keys[0]];
    if (object && keys.length > 1) {
        return getIn(object, keys.slice(1), defaultVal);
    }
    return object === undefined ? defaultVal : object;
}


export const ObjectUtils = {
    isEmpty: (object) => object === undefined || object === null || object === "" || isObjectEmpty(object),
    isObjectEmpty,
    normalizeAccessor,
    normalizeContainerAccessor,
    lastAccessor,
    getIn,
};
