export {emptyFunc, memoize, initDate, updateById, updateByName, updateByProperty, byProperty, byId, byName, bySomeId, any} from "./FuncUtils"
