import {byId, byName, byProperty, updateById, updateByName, updateByProperty} from "./FuncUtils";

describe('FunctionUtils', () => {
    describe('byProperty', () => {
        it('found entity by id property successfully', () => {
            const entity = {
                _id: '1'
            };
            const entityToFind = {
                _id: '1'
            };
            expect(byProperty(entityToFind, '_id')(entity)).toBe(true);
        });
        it('doesn\'t found entity because id property not matches', () => {
            const entity = {
                _id: '1'
            };
            const entityToFind = {
                _id: '2'
            };
            expect(byProperty(entityToFind, '_id')(entity)).toBe(false);
        })
    });
    describe('byId', () => {
        it('found entity by id successfully', () => {
            const entity = {
                _id: '1'
            };
            const entityToFind = {
                _id: '1'
            };
            expect(byId(entityToFind)(entity)).toBe(true);
        });
        it('doesn\'t found entity because id not matches', () => {
            const entity = {
                _id: '1'
            };
            const entityToFind = {
                _id: '2'
            };
            expect(byId(entityToFind)(entity)).toBe(false);
        })
    });
    describe('byName', () => {
        it('found entity by name successfully', () => {
            const entity = {
                name: 'hola'
            };
            const entityToFind = {
                name: 'hola'
            };
            expect(byName(entityToFind)(entity)).toBe(true);
        });
        it('doesn\'t found entity because name not matches', () => {
            const entity = {
                name: 'hola'
            };
            const entityToFind = {
                name: 'chau'
            };
            expect(byName(entityToFind)(entity)).toBe(false);
        })
    });

    describe('updateByProperty', () => {
        it('update array by id property', () => {
            const elements = [
                {_id: '1', name: 'foo'},
                {_id: '2', name: 'bar'},
            ];
            updateByProperty(elements, {_id: '2', name: 'lorem'}, '2', byId('2'))
            expect(elements[1]).toHaveProperty('name', 'lorem');
        })
    });

    describe('updateById', () => {
        it('update array by id', () => {
            const elements = [
                {_id: '1', name: 'foo'},
                {_id: '2', name: 'bar'},
            ];
            updateById(elements, {_id: '2', name: 'lorem'}, '2');
            expect(elements[1]).toHaveProperty('name', 'lorem');
        })
    });

    describe('updateById', () => {
        it('update array by name', () => {
            const elements = [
                {_id: '1', name: 'foo'},
                {_id: '2', name: 'bar'},
            ];
            updateByName(elements, {_id: '2', name: 'lorem'}, 'bar');
            expect(elements[1]).toHaveProperty('name', 'lorem');
        })
    });
})
