import {StrUtils} from "../StrUtils";

export const emptyFunc = () => {};

export function memoize(func) {
    const memo = {};
    const slice = Array.prototype.slice;

    return function () {
        const args = slice.call(arguments);

        if (args in memo)
            return memo[args];
        else
            return (memo[args] = func.apply(this, args));

    }
}

export const initDate = (entity: any) => {
    entity.date = new Date(entity.date)
}

export const byId = (entity: any) => {
    return byProperty(entity, '_id')
}

export const byName = (entity: any) => {
    return byProperty(entity, 'name')
}

export const byProperty = (entity: any, key: string) => {
    const isString = StrUtils.isString(entity);
    if(isString) {
        return (ae: any) => ae[key] === entity;
    }
    return (ae: any) => ae[key] === entity[key]
}

export const updateById = (
    array: any[],
    newEntity: any,
    id: string,
) => {
    return updateByProperty(array, newEntity, id, byId(id));
}

export const updateByName = (
    array: any[],
    newEntity: any,
    name: string,
) => {
    return updateByProperty(array, newEntity, name, byName(name));
}

export const updateByProperty = (
    array: any[],
    newEntity: any,
    property: any,
    comparator: (entity: any) => boolean,
) => {
    const newArray = [...array];
    const index = array.findIndex(comparator);
    if(index < 0) { return newArray};
    Object.assign(newArray[index],newEntity);
    return newArray;
}

export const bySomeId = (values: any[]) => {
    return (value: any) => values.some(someValue => someValue._id === value._id);
}

export const any = (...booleans: any[]) => booleans.some(boolean => !!boolean);
