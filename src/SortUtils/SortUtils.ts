import { StrUtils } from "../StrUtils";

export const dynamicSort = function dynamicSort(property) {
    let sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        let result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
};

export const dynamicSortMultiple = function dynamicSortMultiple() {
    /*
     * save the arguments object as it will be overwritten
     * note that arguments object is an array-like object
     * consisting of the names of the properties to sort by
     */
    let props = arguments;
    return function (obj1, obj2) {
        let i = 0, result = 0, numberOfProperties = props.length;
        /* try getting a different result from 0 (equal)
         * as long as we have extra properties to compare
         */
        while(result === 0 && i < numberOfProperties) {
            result = dynamicSort(props[i])(obj1, obj2);
            i++;
        }
        return result;
    }
};

/**
 * Assums searchText is normalized
 * @param searchText
 * @param property
 */
export const sortPrioritizingStart = (searchText: string, property?: string) => {
    return ((r1: any, r2: any) => {
        if(property) {
            if(!r1[property]) return 1;
            if(!r2[property]) return -1;
        }
        const str1 = StrUtils.normalize(property ? r1[property] : r1);
        const str2 = StrUtils.normalize(property ? r2[property] : r2);
        if(str1.startsWith(searchText)) return -1;
        if(str2.startsWith(searchText)) return 1;
        if(str1 < str2) return -1;
        if(str1 > str2) return 1;
        return 0;
    })
}

export const nameComparator = (a: any, b: any) => a.name.localeCompare(b.name);

export const sortByName = (array: any[]) => {
    array.sort(nameComparator);
    return array;
}
