/**
 * Calcula la cantidad de paginas
 * @param elementsCount
 * @param pageSize
 * @returns {number}
 */
const pageCount = (elementsCount, pageSize) => Math.ceil(elementsCount / pageSize);

/** Ante un cambio en la cantidad de elements, devuelve la pagina correcta segun la pagina actual seleccionada
 * Ejemplo: hay 20 elementos y esta seleccionada la ultima pagina (4), se aplica un filtro y la cantidad
 * de elementos pasa a ser 10, este metodo retornar "2" ya que la pagina "4" ya no existe
 * @param elementsCount
 * @param pageSize
 * @param pageNumber: init in 0.
 * @returns {number}
 */
const normalizePageSelected = (elementsCount, pageSize, pageNumber) => pageCount(elementsCount, pageSize) >= pageNumber + 1 ? pageNumber : 0;
/**
 * Retorna si es necesario mostrar los botones de naveagicion "Anterior" y "Siguiente"
 * @param pageCount
 * @returns {boolean}
 */
const areNavigationNecesary = (pageCount) => pageCount <= 1;
/**
 * En base a los parametros recibidos, calcula que elementos se mostraran en la nueva pagina seleccionada.
 * Retorna un array que se puede utilizar facilmente usando destructuracion en donde se llama a la funcion
 * @param pageNumber
 * @param pageSize
 * @param elements
 * @returns {*}
 */
const pageState = (pageNumber, pageSize, elements) => {
    if (!elements) return {elements, pageNumber};
    pageNumber = normalizePageSelected(elements.length, pageSize, pageNumber);
    let initOfPage = pageNumber * pageSize;
    const elementsOfPage = elements.slice(initOfPage, initOfPage + pageSize);
    return [elementsOfPage, pageNumber];
};


export const PaginationUtils = {
    normalizePageSelected,
    pageCount,
    areNavigationNecesary,
    pageState,
};