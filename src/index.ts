export {removeByProperty, replaceOrPushById, removeByIndex, removeById, range, isLastElement, uniqueElements} from "./ArrayUtils"
export {displayString, displayObject, normalizeDisplay} from "./DisplayUtils"
export {emptyFunc, memoize, initDate, byId, updateById, updateByName, updateByProperty, byProperty, byName, bySomeId, any} from "./FuncUtils"
export {generateDomId} from "./GeneratorUtils";
export {roundToTwoDecimals, roundToNext5, roundToPrev5, multiplesOf5, renderAmount, sum} from "./MathUtils"
export {MyDateUtils} from "./MyDateUtils"
export {getIn, normalizeContainerAccessor, lastAccessor, ObjectUtils, normalizeAccessor, deleteUndefinedProps, isObject, objectToPropertiesArray, objectPropertyToArray, propertyArrayToQueryParam, objectToQueryParam, clone} from "./ObjectUtils"
export {PaginationUtils} from "./PaginationUtils";
export {dynamicSortMultiple, dynamicSort, sortPrioritizingStart, sortByName, nameComparator} from "./SortUtils"
import {StrUtils} from "./StrUtils"
export {StrUtils} from "./StrUtils"
export {isMiddleClick} from "./EventUtils/EventUtils";
export {normalizeFilters} from "./FilterUtils/FilterUtils";

const {
    removeAccents,
    normalize,
    normalizedCompare,
    firstCapitalize,
    lastCaracter,
    removeLastChar,
    isEmpty,
    count,
    isCharNumber,
    toNumber,
    isString,
    stringArrayToText,
    removeEmojis,
    depluralize,
    normalizePluralization,
    extractBetween,
    stringToBoolean,
    numberToRoman
} = StrUtils;

export {
    removeAccents,
    normalize,
    normalizedCompare,
    firstCapitalize,
    lastCaracter,
    removeLastChar,
    isEmpty,
    count,
    isCharNumber,
    toNumber,
    isString,
    stringArrayToText,
    removeEmojis,
    depluralize,
    normalizePluralization,
    extractBetween,
    stringToBoolean,
    numberToRoman
}
