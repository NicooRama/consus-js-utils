/**
 * Elimina el primer elemento que matchea con el valor de la propiedad elegida
 * TODO: modificar para que pueda borrarse segun propiedades anidadas, ejmplo: foo.bar.id
 * @param array
 * @param value
 * @param property
 */
export const removeByProperty = (array: any[], value: any, property: string): void => {
    const index = array.findIndex(element => element[property] === value);
    if(index!==-1) array.splice(index, 1);
};

/**
 * Recibe un elemento y una propiedad sobre la cual comparar, si el elemento
 * existe en el array lo actualiza, sino lo inserta al final del mismo.
 * @param array
 * @param newElement
 * @param idProperty: "_id" por defecto
 */
export const replaceOrPushById = (array: any[], newElement: any, idProperty:string ="_id"): void => {
    const index: number = array.findIndex(element => element[idProperty] === newElement[idProperty]);
    index===-1 ? array.push(newElement) : array[index] = newElement;
};

export const removeById = (array: any[], value:any): void => {
    removeByProperty(array,value,"_id");
};

export const removeByMongoId = (array: any[], value:any): void => {
    removeByProperty(array,value,"_id");
};

export const removeByNormalId = (array: any[], value:any): void => {
    removeByProperty(array,value,"id");
};

/**
 * Elimina el elemento en el indice especificado y lo retorna
 * @param array
 * @param index
 * @returns {T[]}
 */
export const removeByIndex = (array: any[], index: number): any[] => array.splice(index, 1);

export const range = (quantity: number, generator: () => any) => Array.from({length: quantity}, generator)
export const isLastElement = (array: any[], index: number) => (index + 1) >= array.length;

export const uniqueElements = (arr: any[], key = 'id') => {
    const uniques = [];
    return arr.filter(element => {
        const isDuplicate = uniques.includes(element[key]);

        if (!isDuplicate) {
            uniques.push(element[key]);
            return true;
        }
        return false;
    })
};
