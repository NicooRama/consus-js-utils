import expect from 'expect'
import {generateDomId} from "../src/GeneratorUtils/index";

describe('GeneratorUtils', () => {
    describe('generateDomId', () => {
        it('generate consecutives ids correctly', () => {
            const id1 = generateDomId();
            const id2 = generateDomId();
            const id3 = generateDomId();
            expect(id1).toEqual("id0");
            expect(id2).toEqual("id1");
            expect(id3).toEqual("id2");
        })
    });
});