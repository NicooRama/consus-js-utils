import expect from 'expect'
import {removeByIndex, removeByProperty, replaceOrPushById} from "../src/ArrayUtils/index";

describe('ArrayUtils', () => {
    describe('removeByIndex', () => {
        let array;
        beforeEach(() => {
            array = [1 ,2, 3, 4, 5];
        });

        it("remove an element correctly", () => {
            removeByIndex(array,1);
            expect(array).toEqual([1 , 3, 4, 5]);
        });

        it("remove first works correctly", () => {
            removeByIndex(array,0);
            expect(array).toEqual([2, 3, 4, 5]);
        });

        it("remove last works correctly", () => {
            removeByIndex(array,4);
            expect(array).toEqual([1, 2, 3, 4]);
        });

        it("remove by and index that dosent exist dont break", () => {
            const removed = removeByIndex(array,6);
            expect([]).toEqual(removed);
        });
    });

    describe('removeByProperty', () => {
       let array;
       beforeEach(() => {
           array = [
               {foo: 'foo1', bar: 'bar1'},
               {foo: 'foo2', bar: 'bar2'},
               {foo: 'foo3', bar: 'bar3'},
               {foo: 'foo4', bar: 'bar4'},
           ]
       });

        it("remove an element correctly", () => {
            removeByProperty(array, 'bar2', 'bar');
            expect(array).toEqual([
                {foo: 'foo1', bar: 'bar1'},
                {foo: 'foo3', bar: 'bar3'},
                {foo: 'foo4', bar: 'bar4'},
            ]);
        });

        it("remove an element by a property that dosent exist do nothing", () => {
            removeByProperty(array, 'bar2', 'sarasa');
            expect(array).toEqual([
                {foo: 'foo1', bar: 'bar1'},
                {foo: 'foo2', bar: 'bar2'},
                {foo: 'foo3', bar: 'bar3'},
                {foo: 'foo4', bar: 'bar4'},
            ]);
        })
    });

    describe('replaceOrPushById', () => {
        let arrayOne, arrayTwo;
        beforeEach(() => {
            arrayOne = [
                {id: 0, content: 'foo0'},
                {id: 1, content: 'foo1'},
                {id: 2, content: 'foo2'},
                {id: 3, content: 'foo3'},
            ];
            arrayTwo = [
                {_id: 0, content: 'foo0'},
                {_id: 1, content: 'foo1'},
                {_id: 2, content: 'foo2'},
                {_id: 3, content: 'foo3'},
            ]
        });

        it("replace an existing element correctly (specifing property name)", () => {
            const element = {id: 1, content: "newFoo1"};
            replaceOrPushById(arrayOne, element, "id");
            expect(arrayOne).toEqual([
                {id: 0, content: 'foo0'},
                {id: 1, content: 'newFoo1'},
                {id: 2, content: 'foo2'},
                {id: 3, content: 'foo3'},
            ]);
        });

        it("push an unexisting element correctly (specifing property name)", () => {
            const element = {id: 4, content: "foo4"};
            replaceOrPushById(arrayOne, element, "id");
            expect(arrayOne).toEqual([
                {id: 0, content: 'foo0'},
                {id: 1, content: 'foo1'},
                {id: 2, content: 'foo2'},
                {id: 3, content: 'foo3'},
                {id: 4, content: 'foo4'},
            ]);
        });

        it("replace an existing element correctly (default property name)", () => {
            const element = {_id: 1, content: "newFoo1"};
            replaceOrPushById(arrayTwo, element);
            expect(arrayTwo).toEqual([
                {_id: 0, content: 'foo0'},
                {_id: 1, content: 'newFoo1'},
                {_id: 2, content: 'foo2'},
                {_id: 3, content: 'foo3'},
            ]);
        });

        it("push an unexisting element correctly (default property name)", () => {
            const element = {_id: 4, content: "foo4"};
            replaceOrPushById(arrayTwo, element);
            expect(arrayTwo).toEqual([
                {_id: 0, content: 'foo0'},
                {_id: 1, content: 'foo1'},
                {_id: 2, content: 'foo2'},
                {_id: 3, content: 'foo3'},
                {_id: 4, content: 'foo4'},
            ]);
        });
    })

})