import expect from 'expect'
import {normalizeDisplay} from "../src/DisplayUtils/index";

describe('DisplayUtils', () => {
    describe('normalizeDisplay', () => {
        it('display an string correctly', () => {
            const element = "foo";
            const display = normalizeDisplay();
            expect(display(element)).toEqual("foo");
        })

        it('display an object with property method correctly', () => {
            const element = {bar:"foo"};
            const display = normalizeDisplay("bar");
            expect(display(element)).toEqual("foo");
        })

        it('display an object with function method correctly', () => {
            const element = {bar:"foo"};
            const display = normalizeDisplay((item) => item.bar);
            expect(display(element)).toEqual("foo");
        })
    });
});