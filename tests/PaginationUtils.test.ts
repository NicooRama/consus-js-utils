import expect from 'expect'
import {PaginationUtils} from "../src/PaginationUtils/index";

describe('PaginationUtils', () => {
    describe('pageCount', () => {
        it("page count correctly for exactly divition", () => {
           expect(PaginationUtils.pageCount(20,5)).toEqual(4);
        });
        it("page count correctly for no elements", () => {
            expect(PaginationUtils.pageCount(0,5)).toEqual(0);
        });
        it("page count correctly for not exactly divition", () => {
            expect(PaginationUtils.pageCount(22,3)).toEqual(8);
        })
    });

    describe('normalizePageSelected', () => {
        it("normalize correctly for page in page range", () => {
            expect(PaginationUtils.normalizePageSelected(20,5,2)).toEqual(2);
        })
        it("normalize correctly for last page in page range", () => {
            expect(PaginationUtils.normalizePageSelected(20,5,3)).toEqual(3);
        })
        it("normalize correctly for new page out of page range", () => {
            expect(PaginationUtils.normalizePageSelected(20,5,6)).toEqual(0);
        })
    })

    describe('pageState', () => {
        let elements;
        beforeEach(() => {
            elements = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        });

        it('change first page correctly', () => {
           const result = PaginationUtils.pageState(0, 5 , elements);
           expect(result).toEqual([[1,2,3,4,5],0]);
        });
        it('change second page correctly', () => {
            const result = PaginationUtils.pageState(1, 5 , elements);
            expect(result).toEqual([[6, 7, 8, 9, 10],1]);
        });
        it('change third page correctly', () => {
            const result = PaginationUtils.pageState(2, 5 , elements);
            expect(result).toEqual([[11, 12, 13, 14, 15],2]);
        });
        it('change with normalized page correctly', () => {
            const result = PaginationUtils.pageState(4, 5 , elements);
            expect(result).toEqual([[1,2,3,4,5],0]);
        });
    })
});