# @consus/js-utils
### ArrayUtils
```javascript
removeByProperty(array, value, property): void
replaceOrPushById(array, newElement, idProperty = '_id'): void
removeById(array, value): void
removeByMongoId(array, value): void
removeByNormalId(array, value): void
removeByIndex(array, index): any
```

### DisplayUtils
```javascript
displayString(item): string
displayObject(item, property): string
normalizeDisplay(displayForm): string
```

### FuncUtils
```javascript
emptyFunc(): func
memoize(func): func
```

### GenerateUtils
```javascript
generateDomId(): string
```

### MathUtils
```javascript
roundToTwoDecimals(number): number
```

### ObjectUtils
```javascript
ObjectUtils.isEmpty(object): bool
ObjectUtils.isObjectEmpty(object): bool
/** devuelve un valor a traves de un accessor generado por puntos (.)
ObjectUtils.normalizeAccessor(baseObject, accessor, limit = 0): any
/** devuelve el objeto que contiene al valor final **/
ObjectUtils.normalizeContainerAccessor(baseObject, accessor): any
ObjectUtils.lastAccessor(accessor): string
```

### PaginationUtils
```javascript
PaginationUtils.pageCount(elementsCount, pageSize): number
PaginationUtils.normalizePageSelected(elementsCount, pageSize, pageNumber): number
PaginationUtils.areNavigationNecesary(pageCount): bool
PaginationUtils.pageState(pageNumber, pageSize, elements) => [elementsOfPage, pageNumber]
```

### SortUtils
```javascript
dynamicSort(property): func
dynamicSortMultiple(...arg): func
```

### StrUtils
```javascript
StrUtils.removeAccents(str): string
/** Convierte a minusculas y elimina los tildes **/
StrUtils.normalize(str): string
StrUtils.normalizedCompare(str1, str2): bool
StrUtils.firstCapitalize(str): string
StrUtils.lastCaracter(str): string
StrUtils.removeLastChar(str): string
StrUtils.isEmpty(str): bool
StrUtils.count(str, caracter): number
StrUtils.isCharNumber(str): bool
StrUtils.toNumber(str): number
StrUtils.isString(str): bool
```